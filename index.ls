require! {
  sander
  path: {resolve}
  child_process: {exec-file}
}

lsc-bin =
  require \livescript/package.json
  |> (.bin.lsc)
  |> resolve require.resolve(\livescript/package.json), \.., _

module.exports = function livescript-JSON inputdir, outputdir, options, cb
  entry = options?entry or throw new Error "Please provide options.entry"
  dest = options?dest or entry
  space =
    switch options?space
    | true => 2
    | false => undefined
    | _ => that

  entry-file = resolve inputdir, entry
  dest-file = resolve outputdir, dest

  err, stdout, stderr <-! exec-file lsc-bin, [\-pj, entry-file], cwd: inputdir, encoding: \utf8, _
  
  throw err if err
  console.error stderr if stderr
  
  JSON.parse stdout
  |> JSON.stringify _, null, space
  |> sander.write-file dest-file, _, encoding: \utf8
  |> (.then (-> cb!), cb)
